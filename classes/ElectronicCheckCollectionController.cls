public with sharing class ElectronicCheckCollectionController 
{
 
        @AuraEnabled 
    public static Electronic_Check_Collection_Batch__c GetBatch(Id recordId)
    {
     	// query  
      	Electronic_Check_Collection_Batch__c obj = [select id,Action_Requested__c, Batch_Dollar_Amount__c,Batch_Item_Count__c,Name, Batch_Number__c,Branch_Location_Text__c, Date_Scanned__c,Notes__c,Person_Requesting_Action__c,Reason_For_Batch_Action__c	 FROM Electronic_Check_Collection_Batch__c Where id =: recordId];
        return obj;
    }
    
    @AuraEnabled 
    public static Electronic_Check_Collection_Batch_Item__c GetBatchItem(Id recordId)
    {
     	// query  
      	Electronic_Check_Collection_Batch_Item__c obj = [select id,Amount__c,Check_Number__c,Correct_Amount__c, Name,Item_Action__c, Last_Four_of_Item_Account_Number__c,New_Batch_Total__c, Notes__c, Routing_Number__c, Electronic_Check_Collection_Batch__c FROM Electronic_Check_Collection_Batch_Item__c Where Electronic_Check_Collection_Batch__r.Id =: recordId LIMIT 1];
        return obj;
    }


    @AuraEnabled
    public static Boolean GetECCSubmissionDeadlinePassed() 
    {
        // 
                DateTime dt = DateTime.Now();

        
        Boolean bSubmissionDeadlinePassed = true;
        
		bSubmissionDeadlinePassed = GetECCSubmissionDeadlinePassed(dt);        
        return bSubmissionDeadlinePassed;
        


    }
    @AuraEnabled
    public static Boolean GetECCSubmissionDeadlinePassed(DateTime dt) 
    {
        // 
         Boolean bSubmissionDeadlinePassed = true;
        Boolean bDayAllowed = false;
        Boolean bTimeAllowed = false;
        
        //Boolean bMondayAllowed = ECC_Submission__c.getInstance().Monday_Allowed__c;
        //Boolean bTuesdayAllowed = ECC_Submission__c.getInstance().Tuesday_Allowed__c;
        //Boolean bWednesdayAllowed = ECC_Submission__c.getInstance().Wednesday_Allowed__c;
		//Boolean bThursdayAllowed = ECC_Submission__c.getInstance().Thursday_Allowed__c;
        //Boolean bFridayAllowed = ECC_Submission__c.getInstance().Friday_Allowed__c;
        //Boolean bSaturdayAllowed = ECC_Submission__c.getInstance().Saturday_Allowed__c;
        //Boolean bSundayAllowed = ECC_Submission__c.getInstance().Sunday_Allowed__c;
        
        //decimal dMondayStartTime = ECC_Submission__c.getInstance().Monday_Start_Time__c;
        //decimal dTuesdayStartTime = ECC_Submission__c.getInstance().Tuesday_Start_Time__c;
        //decimal dWednesdayStartTime = ECC_Submission__c.getInstance().Wednesday_Start_Time__c;
        //decimal dThursdayStartTime = ECC_Submission__c.getInstance().Thursday_Start_Time__c;
        //decimal dFridayStartTime = ECC_Submission__c.getInstance().Friday_Start_Time__c;
        //decimal dSaturdayStartTime = ECC_Submission__c.getInstance().Saturday_Start_Time__c;
        //decimal dSundayStartTime = ECC_Submission__c.getInstance().Sunday_Start_Time__c;

        //decimal dMondayEndTime = ECC_Submission__c.getInstance().Monday_End_Time__c;
        //decimal dTuesdayEndTime = ECC_Submission__c.getInstance().Tuesday_End_Time__c;
        //decimal dWednesdayEndTime = ECC_Submission__c.getInstance().Wednesday_End_Time__c;
        //decimal dThursdayEndTime = ECC_Submission__c.getInstance().Thursday_End_Time__c;
        //decimal dFridayEndTime = ECC_Submission__c.getInstance().Friday_End_Time__c;
        //decimal dSaturdayEndTime = ECC_Submission__c.getInstance().Saturday_End_Time__c;
        //decimal dSundayEndTime = ECC_Submission__c.getInstance().Sunday_End_Time__c;
        
        //DateTime dt = DateTime.Now();
        integer iCurrentHour = dt.hour();
       	Integer iDayOfWeek = 0;
        // next two lines determines day of week
        //Integer iTotalDays = System.Date.NewInstance( dt.Year(), dt.Month(), dt.Day()).daysBetween(System.Date.NewInstance( 1900, 1, 7 ));
        Integer iTotalDays = System.Date.NewInstance( 1900, 1, 7 ).daysBetween(System.Date.NewInstance( dt.Year(), dt.Month(), dt.Day()));
        iDayOfWeek = math.mod( iTotalDays, 7 );
        
        System.Debug('current hour '  + iCurrentHour);
        System.Debug('day of week '  + iDayOfWeek);

        //CASE(
        //  MOD( DateTime.Now() - DATE( 1900, 1, 7 ), 7 ),
        //  0, "Sunday",
        //  1, "Monday",
        //  2, "Tuesday",
        //  3, "Wednesday",
        //  4, "Thursday",
        //  5, "Friday", 
        //  "Saturday"
        //)

        //
        if (iDayOfWeek == 0)
        {
        	//Sunday
        	bDayAllowed = ECC_Submission__c.getInstance().Sunday_Allowed__c;
        	decimal dStartTime = ECC_Submission__c.getInstance().Sunday_Start_Time__c;
        	decimal dEndTime = ECC_Submission__c.getInstance().Sunday_End_Time__c;

        	if ((iCurrentHour >= dStartTime) && (iCurrentHour < dEndTime))
            {
             	bTimeAllowed = true;   
            }        	
        }
		else if (iDayOfWeek == 1)
        {
        	//Monday
        	bDayAllowed = ECC_Submission__c.getInstance().Monday_Allowed__c;
        	decimal dStartTime = ECC_Submission__c.getInstance().Monday_Start_Time__c;
        	decimal dEndTime = ECC_Submission__c.getInstance().Monday_End_Time__c;

        	if ((iCurrentHour >= dStartTime) && (iCurrentHour < dEndTime))
            {
             	bTimeAllowed = true;

            }        	
        }
 		else if (iDayOfWeek == 2)
        {
        	//Tuesday
        	bDayAllowed = ECC_Submission__c.getInstance().Tuesday_Allowed__c;
        	decimal dStartTime = ECC_Submission__c.getInstance().Tuesday_Start_Time__c;
        	decimal dEndTime = ECC_Submission__c.getInstance().Tuesday_End_Time__c;

        	if ((iCurrentHour >= dStartTime) && (iCurrentHour < dEndTime))
            {
             	bTimeAllowed = true;   
            }        	
        }
		else if (iDayOfWeek == 3)
        {
        	//Wednesday
        	bDayAllowed = ECC_Submission__c.getInstance().Wednesday_Allowed__c;
        	decimal dStartTime = ECC_Submission__c.getInstance().Wednesday_Start_Time__c;
        	decimal dEndTime = ECC_Submission__c.getInstance().Wednesday_End_Time__c;

        	if ((iCurrentHour >= dStartTime) && (iCurrentHour < dEndTime))
            {
             	bTimeAllowed = true;   
            }        	
        }
		else if (iDayOfWeek == 4)
        {
        	//Thursday
        	bDayAllowed = ECC_Submission__c.getInstance().Thursday_Allowed__c;
        	decimal dStartTime = ECC_Submission__c.getInstance().Thursday_Start_Time__c;
        	decimal dEndTime = ECC_Submission__c.getInstance().Thursday_End_Time__c;

        	if ((iCurrentHour >= dStartTime) && (iCurrentHour < dEndTime))
            {
             	bTimeAllowed = true;   
            }        	
        }
		else if (iDayOfWeek == 5)
        {
        	//Friday
        	bDayAllowed = ECC_Submission__c.getInstance().Friday_Allowed__c;
        	decimal dStartTime = ECC_Submission__c.getInstance().Friday_Start_Time__c;
        	decimal dEndTime = ECC_Submission__c.getInstance().Friday_End_Time__c;

        	if ((iCurrentHour >= dStartTime) && (iCurrentHour < dEndTime))
            {
             	bTimeAllowed = true;   
            }        	
        }
		else if (iDayOfWeek == 6)
        {
        	//Saturday
        	bDayAllowed = ECC_Submission__c.getInstance().Saturday_Allowed__c;
        	decimal dStartTime = ECC_Submission__c.getInstance().Saturday_Start_Time__c;
        	decimal dEndTime = ECC_Submission__c.getInstance().Saturday_End_Time__c;

        	if ((iCurrentHour >= dStartTime) && (iCurrentHour < dEndTime))
            {
             	bTimeAllowed = true;   
            }        	
        }
       
        
        // add logic here
        // 
        if ((bDayAllowed == true) && (bTimeAllowed == true))
        {
         	bSubmissionDeadlinePassed = false;   
        }
         System.Debug('bSubmissionDeadlinePassed =  '  + bSubmissionDeadlinePassed);

        return bSubmissionDeadlinePassed;
        


    }
    
    
    @AuraEnabled 
    public static Contact fetchContact(Id objContactId)
    {
     	// query  
      	Contact objContact = [select id,AccountId,Email,Name, Phone FROM Contact Where id =: objContactId];
        return objContact;
    }
    @AuraEnabled 
    public static Account fetchContactAccount(Id objContactId)
    {
     	// query  
      	Contact objContact = [select id,AccountId,Email,Name, Phone FROM Contact Where id =: objContactId];
 
     	Id accountid = '001L0000014m8IUIAY'; 	// temp hack
        if (objContact.AccountId != null)
        {
         	accountid = objContact.AccountId;   
        }
        else
        {
            // Get a list in case using more than one of the same emails as a temp hack
            //List<Contact> objContactLst = [select id,Name,Email, AccountId 
            //         FROM Contact Where Email =: oUser.Email];
            //for (Contact objContact : objContactLst)
            //{
                           
            //    if (objContact.AccountId != null)
            //    {            
            //           accountid = objContact.AccountId;
            //    }
            //}
                  
        }
      	Account objAccount = [select id,Name FROM Account Where id =: accountid];
        
        return objAccount;
    }

    @AuraEnabled 
    public static user fetchUser()
    {
     // query current user information  
      User oUser = [select id,Name,TimeZoneSidKey,Username,Alias,Country,Email,FirstName,LastName,IsActive,IsPortalEnabled, ContactId 
                 FROM User Where id =: userInfo.getUserId()];
        return oUser;
    }

    @AuraEnabled 
    public static Account fetchUserAccount()
    {
     	// query current user information
      	User oUser = [select id,Name,TimeZoneSidKey,Username,Alias,Country,Email,FirstName,LastName,IsActive,IsPortalEnabled, ContactId, AccountId 
                 FROM User Where id =: userInfo.getUserId()];
 
     	Id accountid = '001L0000014m8IUIAY'; 	// temp hack
        if (oUser.AccountId != null)
        {
         	accountid = oUser.AccountId;   
        }
        else
        {
            // Get a list in case using more than one of the same emails as a temp hack
            List<Contact> objContactLst = [select id,Name,Email, AccountId 
                     FROM Contact Where Email =: oUser.Email];
            for (Contact objContact : objContactLst)
            {
                           
                if (objContact.AccountId != null)
                {            
                       accountid = objContact.AccountId;
                }
            }
                  
        }
      	Account objAccount = [select id,Name FROM Account Where id =: accountid];
        
        return objAccount;
    }

   

    @AuraEnabled
    public static Electronic_Check_Collection_Batch__c SaveBatch(Electronic_Check_Collection_Batch__c obj, 
                                                                 List<Electronic_Check_Collection_Batch_Item__c> objItemList,
                                                                 ID objCaseId, ID objContactId)
    {
        // 
        //
        string strAccountName = '';
        ID iAccountId;
       // ID iContactId = objCase.ContactId;
        //ID iCaseId = objCaseId;
        ID iContactId = objContactId;
        ID iCaseId = objCaseId;
        string strBranchLocationText = obj.Branch_Location_Text__c;
        
        system.debug('In SaveBatch********************************************************************************************************' );

        //string strSubject = objCase.Subject;
        //string strDescription = objCase.Description;
        //string strServiceArea = objCase.Service_Area__c;
        //string strCategory = objCase.Category__c;
        //string strSubCategory = objCase.Sub_Category__c;
        
        Boolean bSubmissionPassed = GetECCSubmissionDeadlinePassed();
        if (bSubmissionPassed)
        {
         	if(Test.isRunningTest())
            {}
            else
            {
            	CalloutException e = new CalloutException();
            	e.setMessage('Items can only be deleted/corrected the same day they are received by Synergent');
            	throw e;
            }
        }
        
        Account objAccount = fetchContactAccount(iContactId);
        iAccountId = objAccount.Id;
        
        /// User objUser = [select id,Name,Username,Email,FirstName,LastName, ContactId, AccountId FROM User Where id =: userInfo.getUserId()];
        ///if ((objUser.ContactId != null) && (objUser.AccountId != null))
        //{        
        //        obj.AccountId__c = objUser.AccountId;
        //        Account objAccount = [select Name from Account Where id =: objUser.AccountId]; 
        //        strAccountName = objAccount.Name;
        //        iAccountId = objUser.AccountId;
        //    	iContactId = objUser.ContactId;            
        //}
        //else
        //{
                // assume user is internal and not in contact table
            	//string strRequestLastName = strRequestorName;
               // string strRequestFirstName = '';
            	//ID iRequestAccountId;
            	//List<Contact> objContactList = [select id, AccountId from Contact Where LastName =: strRequestLastName AND FirstName =: strRequestFirstName];
            	//List<Contact> objContactList = [select id, AccountId, LastName, FirstName from Contact Where LastName =: strRequestLastName];
            	//for (Contact objContact : objContactList)
				//{
                //	if (objContact.AccountId != null)
                //    {
                //    	iRequestAccountId = objContact.AccountId;
                        //Contact objContactHack = [select id, AccountId from Contact Where Email =: objUser.Email];
                //		obj.AccountId__c = iRequestAccountId;  
                //		iContactId = objContact.Id;
            	//		Account objAccount = [select Name from Account Where id =: iRequestAccountId];
                 //       if (objAccount != null)
                 //       {
                //			strAccountName = objAccount.Name;
                //			iAccountId = iRequestAccountId;
                //    	}   
                //    }            
           		//}            
       // }
               
        //obj.Branch_Location_Text__c = strBranchLocationText;
        //
        //
        obj.Create_Date__c = System.Date.today();
        obj.Person_Requesting_Action__c = iContactId;
        //
        obj.Case__c = iCaseId;
        obj.AccountId__c = iAccountId;
        obj.Name = 'ECC Submission Batch';
        
        //Save batch first as process will kick off when I create the case
        insert obj;
        
                system.debug('Inserted  Batch********************************************************************************************************' );

        //system.debug("Create batch item array: " + JSON.stringify(objItemList));
        //Item here
        for(Electronic_Check_Collection_Batch_Item__c objItem : objItemList)
        {
            // set the batch id in the item to set related 
        	objItem.Electronic_Check_Collection_Batch__c = obj.Id;
            objItem.Name = obj.Name + ' Item';
                             
            if ((obj.Action_Requested__c == 'Batch Delete')||(obj.Action_Requested__c == 'Item Delete'))
            {
            	objItem.Item_Action__c = 'Delete';    
            }
            else if (obj.Action_Requested__c == 'Item Correction')
            {
            	objItem.Item_Action__c = 'Correct' ;
            }
        }
        // only add if an item type, not batch
        if ((obj.Action_Requested__c == 'Item Correction')||(obj.Action_Requested__c == 'Item Delete'))
        {
         	//update list now
        	insert objItemList;
                            system.debug('Inserted  Batch Items********************************************************************************************************' );

      	}
        
        // Create the case object here
        //Case objNewCase = new Case();
        //objNewCase.Subject = strSubject;
        //objNewCase.Description = strDescription;
        //objNewCase.Service_Area__c = strServiceArea;
        //objNewCase.Category__c = strCategory;
        // bug here , need to be able to select none
        //objNewCase.Sub_Category__c = strSubCategory;
        
        //objNewCase.Subject =   strAccountName + ' ' + 'ECC Form Submission';
        //objNewCase.Description = 'Form submitted thru community portal.';
        //objNewCase.Status = 'New';
        //objNewCase.Type = 'Customer Service Request';
        // maybe not 
        //objNewCase.Origin = 'Synergent Case Portal';
        //objNewCase.Product_Type__c = 'Check_Processing';
        //objNewCase.AccountId = iAccountId;
                
        //Id idRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('External Service Request').getRecordTypeId();
        //objNewCase.RecordTypeId = idRecordTypeId;
        //insert objNewCase;
        
        //Now get the case object id and update here to get the related link to work
        //Electronic_Check_Collection_Batch__c objGet = [select Id, Case__c from Electronic_Check_Collection_Batch__c where Id=: obj.Id];
        //objGet.Case__c = objNewCase.Id;
        //objGet.Case__c = iCaseId;
        //update objGet;
        
        system.debug('Exiting SaveBatch********************************************************************************************************' );
        
        return obj;

    }
}