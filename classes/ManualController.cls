public class ManualController 
{
    @AuraEnabled
    public static Manual__c Save(Manual__c obj, ID objCaseId,ID objContactId)
    {
        ID iContactId = objContactId;
        ID iCaseId = objCaseId;
        
        system.debug('In Manual Save********************************************************************************************************' );

        //
        obj.Case__c = iCaseId;
        
        obj.Name = 'Manual Submission';
        
        insert obj;
        
         system.debug('Inserted  Manual********************************************************************************************************' );
        
        system.debug('Exiting Manual Save********************************************************************************************************' );
        
        return obj;

    }

            @AuraEnabled 
    public static Manual__c GetManual(Id recordId)
    {
     	// query  
      	Manual__c obj = [select id,Name	 FROM Manual__c Where id =: recordId];
        return obj;
    }

}