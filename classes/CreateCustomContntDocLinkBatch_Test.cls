/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy     Description
*     1.0        120619         BFL Users     This class is built to test CreateCustomContentDocumentLinkBatch apex class
**********************************************************************************************************************************************************/ 
@isTest
public class CreateCustomContntDocLinkBatch_Test {
    
    @testSetup
    public static void setupTestData() {
    
        // Variable declarations

        // For account
        Integer accRecCount = 5;
        List<Account> accInsertionList = new List<Account>();
        // For contact
        Integer conRecCount = 5;
        List<Contact> conInsertionList = new List<Contact>();
        // For case
        Integer caseRecCount = 5;
        List<Case> caseInsertionList = new List<Case>();
        // For ContentVersion
        Integer contentVerRecCount = 5;
        List<ContentVersion> contentVerInsertionList = new List<ContentVersion>();
        // For ContentDocumentLink
        Integer contentDocLinkRecCount = 5;
        List<ContentDocumentLink> contentDocLinkInsertionList = new List<ContentDocumentLink>();
        // For Case Monitoring
        Integer caseMonRecCount = 5;
        List<Case_Monitoring__c> caseMonInsertionList = new List<Case_Monitoring__c>();
        
        // Insert Account
        for(Integer i=0; i < accRecCount; i++) {
            Account objAcc = new Account();
            objAcc.Name = 'Test Account'+i;
            accInsertionList.add(objAcc);
        }
        insert accInsertionList;
        
        // Insert Contact
        for(Integer i=0; i < conRecCount; i++) {
            Contact objContact = new Contact();
            objContact.FirstName = 'Test';
            objContact.LastName = 'Contact'+i;
            if(accInsertionList.size() >= i) {
                objContact.AccountId = accInsertionList[i].Id;
            } else if(!accInsertionList.isEmpty()) {
                objContact.AccountId = accInsertionList[0].Id;
            }
            conInsertionList.add(objContact);
        }
        insert conInsertionList;
        
        // Insert Case
        Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();
        System.debug('RecordTypeIdCase======'+RecordTypeIdCase);
        for(Integer i=0; i < caseRecCount; i++) {
            Case objCase = new Case();
            objCase.Subject = 'Test Subject'+i;
            objCase.Description = 'Test Description'+i;
            objCase.recordtypeid = RecordTypeIdCase;
            objCase.Status = 'New';
            objCase.Origin = 'Email';
            objCase.Date_Required__c = Date.today();
            if(conInsertionList.size() >= i) {
                objCase.ContactId = conInsertionList[i].Id;
                //objCase.AccountId = conInsertionList[i].AccountId;
            } else if(!conInsertionList.isEmpty()) {
                objCase.ContactId = conInsertionList[0].Id;
                //objCase.AccountId = conInsertionList[0].AccountId;
            }
            caseInsertionList.add(objCase);
        }
        insert caseInsertionList;

        // Create a dummy case record for negative testing
        Case objDummyCase = new Case();
        objDummyCase.Subject = 'Dummy Case Subject';
        objDummyCase.Description = 'Dummy Case Description';
        objDummyCase.recordtypeid = RecordTypeIdCase;
        objDummyCase.Status = 'New';
        objDummyCase.Origin = 'Email';
        objDummyCase.Date_Required__c = Date.today();
        objDummyCase.ContactId = conInsertionList[0].Id;
        insert objDummyCase;
        
        
        
        // Insert ContentVersion
        for(Integer i=0; i < contentVerRecCount; i++) {
            ContentVersion objContentVersion = new ContentVersion();
            objContentVersion.Title = 'Test Title';
            objContentVersion.PathOnClient = 'Test.jpg';
            objContentVersion.VersionData = Blob.valueOf('Test Content Data');
            objContentVersion.IsMajorVersion = true;
            contentVerInsertionList.add(objContentVersion);
        }
        insert contentVerInsertionList;
        
        // Fetch Content Document records
        List<ContentDocument> documents = [SELECT Id, 
                                                  Title, 
                                                  LatestPublishedVersionId 
                                             FROM ContentDocument];
        System.debug('documents======='+documents);  

        // Insert ContentDocumentLink
        for(Integer i=0; i < contentDocLinkRecCount; i++) {
            ContentDocumentLink objContentDocLink = new ContentDocumentLink();
            if(caseInsertionList.size() >= i) {
                objContentDocLink.LinkedEntityId = caseInsertionList[i].Id;
            } 
            if(documents.size() >= i) {
                objContentDocLink.ContentDocumentId = documents[i].Id;
            } 
            objContentDocLink.Visibility = 'AllUsers';
            objContentDocLink.ShareType= 'V';
            contentDocLinkInsertionList.add(objContentDocLink);
        }  
        insert contentDocLinkInsertionList; 
        
        
    }
    
    @isTest
    public static void positiveTestCustomConDocLink(){
        List<Case> listCase = new List<Case>();
        listCase = [SELECT Id
                      FROM Case];
        System.debug('listCase======='+listCase+'==========listCase size======='+listCase.size());
        
        List<ContentDocument> documents = [SELECT Id, 
                                           Title, 
                                           LatestPublishedVersionId 
                                           FROM ContentDocument];
        System.debug('documents======='+documents);  
        
        Test.startTest();
        // call batch class to execute the business logic.
        CreateCustomContentDocumentLinkBatch customDocLinkObj = new CreateCustomContentDocumentLinkBatch();
        Database.executeBatch(customDocLinkObj);
        Test.stopTest();
        
        // get the list of Custom content document link 
        List<Custom_Content_Document_Link__c>  customDocLinkList = [SELECT Id
                                                                    FROM Custom_Content_Document_Link__c];
        System.debug('--customDocLinkObj--'+customDocLinkList+'==customDocLinkList size==='+customDocLinkList.size());
        
        System.assertEquals(5, customDocLinkList.size());
        
    }
    
    
}