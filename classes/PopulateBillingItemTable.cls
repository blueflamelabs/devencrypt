public class PopulateBillingItemTable 
{
	public static void AutoInsert()
	{
    	// Create the object here
        List<Billing_Item__c> objList = new List<Billing_Item__c>();
        
        //
        Billing_Item__c obj = new Billing_Item__c();
        obj.Name = 'Inclearing Items';
        obj.Description__c = '';
        obj.Cost__c = 0.013;
        obj.Category__c = 'Inclearing';
        obj.Item_Number__c	 = '4670-000-000';
        objList.Add(obj);
        //

        Billing_Item__c obj2 = new Billing_Item__c();
        obj2.Name = 'OTC Image';
        obj2.Description__c = '';
        obj2.Cost__c = 0.015;
        obj2.Category__c = 'Image';
        obj2.Item_Number__c	 = '4671-000-000';
        objList.Add(obj2);

        //
        Billing_Item__c obj3 = new Billing_Item__c();
        obj3.Name = 'Image Capture';
        obj3.Description__c = '';
        obj3.Cost__c = 0.002;
        obj3.Category__c = 'Image';
        obj3.Item_Number__c	 = '4671-000-003';
        objList.Add(obj3);
        //

        Billing_Item__c obj4 = new Billing_Item__c();
        obj4.Name = 'Home Banking Iamges';
        obj4.Description__c = '';
        obj4.Cost__c = 0.00;
        obj4.Category__c = 'Image';
        obj4.Item_Number__c	 = '4671-000-002';
        objList.Add(obj4);

        //
        Billing_Item__c obj5 = new Billing_Item__c();
        obj5.Name = 'Internet Check Copy';
        obj5.Description__c = '';
        obj5.Cost__c = 0.00;
        obj5.Category__c = 'Image';
        obj5.Item_Number__c	 = '4671-000-001';
        objList.Add(obj5);
        //

        Billing_Item__c obj6 = new Billing_Item__c();
        obj6.Name = 'Return Items';
        obj6.Description__c = '';
        obj6.Cost__c = 1.75;
        obj6.Category__c = 'Returns';
        obj6.Item_Number__c	 = '4672-000-000';
        objList.Add(obj6);

        //
        Billing_Item__c obj7 = new Billing_Item__c();
        obj7.Name = 'Reg J Return';
        obj7.Description__c = '';
        obj7.Cost__c = 5.00;
        obj7.Category__c = 'Returns';
        obj7.Item_Number__c	 = '4672-000-001';
        objList.Add(obj7);
        //

        Billing_Item__c obj8 = new Billing_Item__c();
        obj8.Name = 'On-Us Processing';
        obj8.Description__c = '';
        obj8.Cost__c = 0.045;
        obj8.Category__c = 'Check Collection';
        obj8.Item_Number__c	 = '4673-000-000';
        objList.Add(obj8);
       
        //
        Billing_Item__c obj9 = new Billing_Item__c();
        obj9.Name = 'Direct Exchange Processing';
        obj9.Description__c = '';
        obj9.Cost__c = 0.045;
        obj9.Category__c = 'Check Collection';
        obj9.Item_Number__c	 = '4673-000-001';
        objList.Add(obj9);
        //

        Billing_Item__c obj10 = new Billing_Item__c();
        obj10.Name = 'Processing';
        obj10.Description__c = '';
        obj10.Cost__c = 0.055;
        obj10.Category__c = 'Check Collection';
        obj10.Item_Number__c	 = '4673-000-002';
        objList.Add(obj10);

        //
        Billing_Item__c obj11 = new Billing_Item__c();
        obj11.Name = 'Items Returned/Charged Back';
        obj11.Description__c = '';
        obj11.Cost__c = 1.00;
        obj11.Category__c = 'Check Collection';
        obj11.Item_Number__c	 = '4673-000-003';
        objList.Add(obj11);
        //

        Billing_Item__c obj12 = new Billing_Item__c();
        obj12.Name = 'Redeposit Chargeback';
        obj12.Description__c = '';
        obj12.Cost__c = 0.50;
        obj12.Category__c = 'Check Collection';
        obj12.Item_Number__c	 = '4673-000-004';
        objList.Add(obj12);

        //
        Billing_Item__c obj13 = new Billing_Item__c();
        obj13.Name = 'Can/SB/NCI Processing';
        obj13.Description__c = '';
        obj13.Cost__c = 0.00;
        obj13.Category__c = 'Check Collection';
        obj13.Item_Number__c	 = '4673-000-005';
        objList.Add(obj13);
        //

        Billing_Item__c obj14 = new Billing_Item__c();
        obj14.Name = 'Batch Deletions';
        obj14.Description__c = '';
        obj14.Cost__c = 3.00;
        obj14.Category__c = 'Check Collection';
        obj14.Item_Number__c	 = '4673-000-006';
        objList.Add(obj14);

        //
        Billing_Item__c obj15 = new Billing_Item__c();
        obj15.Name = 'Duplicate Batch Correction (1-50)';
        obj15.Description__c = '';
        obj15.Cost__c = 20.00;
        obj15.Category__c = 'Check Collection';
        obj15.Item_Number__c	 = '4673-000-007';
        objList.Add(obj15);
        //

        Billing_Item__c obj16 = new Billing_Item__c();
        obj16.Name = 'Duplicate Batch Correction (50-100)';
        obj16.Description__c = '';
        obj16.Cost__c = 40.00;
        obj16.Category__c = 'Check Collection';
        obj16.Item_Number__c	 = '4673-000-007';
        objList.Add(obj16);
        //
        Billing_Item__c obj17 = new Billing_Item__c();
        obj17.Name = 'Duplicate Batch Correction (100-up)';
        obj17.Description__c = '';
        obj17.Cost__c = 60.00;
        obj17.Category__c = 'Check Collection';
        obj17.Item_Number__c	 = '4673-000-007';
        objList.Add(obj17);
        //

        Billing_Item__c obj18 = new Billing_Item__c();
        obj18.Name = 'Overnight Postage (Can/SB/NCI)';
        obj18.Description__c = '';
        obj18.Cost__c = 0.00;
        obj18.Category__c = 'Check Collection';
        obj18.Item_Number__c	 = '4673-000-008';
        objList.Add(obj18);

        //
        Billing_Item__c obj19 = new Billing_Item__c();
        obj19.Name = 'Adjustment';
        obj19.Description__c = '';
        obj19.Cost__c = 3.00;
        obj19.Category__c = 'Research';
        obj19.Item_Number__c	 = '4674-000-000';
        objList.Add(obj19);
        //

        Billing_Item__c obj20 = new Billing_Item__c();
        obj20.Name = 'Research Request';
        obj20.Description__c = '';
        obj20.Cost__c = 1.00;
        obj20.Category__c = 'Research';
        obj20.Item_Number__c	 = '4674-000-001';
        objList.Add(obj20);

        //
        Billing_Item__c obj21 = new Billing_Item__c();
        obj21.Name = 'Research Request-Hourly Fee (over 15 items)';
        obj21.Description__c = '';
        obj21.Cost__c = 15.00;
        obj21.Category__c = 'Research';
        obj21.Item_Number__c	 = '4674-000-001';
        objList.Add(obj21);
        //

        Billing_Item__c obj22 = new Billing_Item__c();
        obj22.Name = 'Manual Handling';
        obj22.Description__c = '';
        obj22.Cost__c = 2.50;
        obj22.Category__c = 'Research';
        obj22.Item_Number__c	 = '4674-000-002';
        objList.Add(obj22);

        //
        Billing_Item__c obj23 = new Billing_Item__c();
        obj23.Name = 'Rush Research Request';
        obj23.Description__c = '';
        obj23.Cost__c = 5.00;
        obj23.Category__c = 'Research';
        obj23.Item_Number__c	 = '4674-000-003';
        objList.Add(obj23);
        //

        Billing_Item__c obj24 = new Billing_Item__c();
        obj24.Name = 'Monthly Transmission/Settlement Fee';
        obj24.Description__c = '';
        obj24.Cost__c = 300.00;
        obj24.Category__c = 'Miscellaneous';
        obj24.Item_Number__c	 = '4675-000-001';
        objList.Add(obj24);
        //
        Billing_Item__c obj25 = new Billing_Item__c();
        obj25.Name = 'Monthly Wire Fee';
        obj25.Description__c = '';
        obj25.Cost__c = 50.00;
        obj25.Category__c = 'Miscellaneous';
        obj25.Item_Number__c	 = '4675-000-004';
        objList.Add(obj25);
        //

        Billing_Item__c obj26 = new Billing_Item__c();
        obj26.Name = 'Monthly Minimum';
        obj26.Description__c = '';
        obj26.Cost__c = 0.00;
        obj26.Category__c = 'Miscellaneous';
        obj26.Item_Number__c	 = '4675-000-000';
        objList.Add(obj26);

        //
        Billing_Item__c obj27 = new Billing_Item__c();
        obj27.Name = 'Credit Adjustment';
        obj27.Description__c = '';
        obj27.Cost__c = 0.00;
        obj27.Category__c = 'Miscellaneous';
        obj27.Item_Number__c	 = '4675-000-003';
        objList.Add(obj27);
        //

        Billing_Item__c obj28 = new Billing_Item__c();
        obj28.Name = 'Postage';
        obj28.Description__c = '';
        obj28.Cost__c = 0.00;
        obj28.Category__c = 'Miscellaneous';
        obj28.Item_Number__c	 = '4389-000-002';
        objList.Add(obj28);

        //
        Billing_Item__c obj29 = new Billing_Item__c();
        obj29.Name = 'ECC-Hardware/Software';
        obj29.Description__c = '';
        obj29.Cost__c = 0.00;
        obj29.Category__c = 'Miscellaneous';
        obj29.Item_Number__c	 = '4690-000-002';
        objList.Add(obj29);
        //

        Billing_Item__c obj30 = new Billing_Item__c();
        obj30.Name = 'Annual V-Soft Maintenance';
        obj30.Description__c = '';
        obj30.Cost__c = 0.00;
        obj30.Category__c = 'Miscellaneous';
        obj30.Item_Number__c	 = '4691-000-001';
        objList.Add(obj30);

        //
        Billing_Item__c obj31 = new Billing_Item__c();
        obj31.Name = 'SB Check Image Portal';
        obj31.Description__c = '';
        obj31.Cost__c = 100.00;
        obj31.Category__c = 'Miscellaneous';
        obj31.Item_Number__c	 = '4675-000-002';
        objList.Add(obj31);
        //

        Billing_Item__c obj32 = new Billing_Item__c();
        obj32.Name = 'Synergent Implementation Fee';
        obj32.Description__c = '';
        obj32.Cost__c = 0.00;
        obj32.Category__c = 'Miscellaneous';
        obj32.Item_Number__c	 = '4690-000-003';
        objList.Add(obj32);
        //

        insert objList;
	}
        
}