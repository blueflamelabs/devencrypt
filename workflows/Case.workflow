<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CIS_New_assignment_notification</fullName>
        <description>New assignment notification - CIS</description>
        <protected>false</protected>
        <recipients>
            <recipient>CIS</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>customerservice@synergentcorp.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Emails/SUPPORTNewassignmentnotification</template>
    </alerts>
    <alerts>
        <fullName>CM_New_assignment_notification</fullName>
        <description>New assignment notification - CM</description>
        <protected>false</protected>
        <recipients>
            <recipient>Change_Management</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>customerservice@synergentcorp.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Emails/SUPPORTNewassignmentnotification</template>
    </alerts>
    <alerts>
        <fullName>CP_Collection_Queue_Notification</fullName>
        <description>CP Collection Queue Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>CP_Collection_Group</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>customerservice@synergentcorp.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Emails/SUPPORTNewassignmentnotification</template>
    </alerts>
    <alerts>
        <fullName>CP_Inclearing_Queue_Notification</fullName>
        <description>CP Inclearing Queue Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>CP_Inclearing_Group</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>customerservice@synergentcorp.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Emails/SUPPORTNewassignmentnotification</template>
    </alerts>
    <alerts>
        <fullName>CP_Returns_Queue_Notification</fullName>
        <description>CP Returns Queue Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>CP_Returns_Group</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>customerservice@synergentcorp.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Emails/SUPPORTNewassignmentnotification</template>
    </alerts>
    <alerts>
        <fullName>Case_Cancelled_Notification_to_Contact</fullName>
        <description>Case Cancelled Notification to Contact</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>customerservice@synergentcorp.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Emails/Cancelled_Case_Notification_to_Contact</template>
    </alerts>
    <alerts>
        <fullName>Case_Closed</fullName>
        <description>Case Closed Notification to Contact</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer Colleague</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>customerservice@synergentcorp.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Emails/Closed_Case_Notification_to_Contact</template>
    </alerts>
    <alerts>
        <fullName>Case_Comment_Notification_to_Case_Owner</fullName>
        <description>Case Comment Notification to Case Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>customerservice@synergentcorp.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Emails/Case_Comment_Notification_to_Case_Owner</template>
    </alerts>
    <alerts>
        <fullName>Case_Comment_Notification_to_Contacts</fullName>
        <description>Case Comment Notification to Contacts</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer Colleague</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>customerservice@synergentcorp.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Emails/CaseCommentNotificationToCustomerCaseTeam</template>
    </alerts>
    <alerts>
        <fullName>Customer_Service_Daily_Queue_Notification</fullName>
        <description>Customer Service Daily Queue Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer_Service_Daily_Group</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>customerservice@synergentcorp.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Emails/SUPPORTNewassignmentnotification</template>
    </alerts>
    <alerts>
        <fullName>Customer_Service_New_assignment_notification</fullName>
        <description>New assignment notification - Customer Service</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer_Service_Group</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>customerservice@synergentcorp.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Emails/SUPPORTNewassignmentnotification</template>
    </alerts>
    <alerts>
        <fullName>Invoicing_in_Progress_Email_Alert</fullName>
        <description>Invoicing in Progress Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>CP_Billing</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>customerservice@synergentcorp.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Emails/ESR_Invoice_Notification_to_Billing</template>
    </alerts>
    <alerts>
        <fullName>New_Assignment_Notification_Check_Processing</fullName>
        <description>New Assignment Notification CP Collection</description>
        <protected>false</protected>
        <recipients>
            <recipient>CP_Collection_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Emails/SUPPORTNewassignmentnotification</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Notification_to_Contacts</fullName>
        <description>New Case  Notification to Contacts</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer Colleague</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>customerservice@synergentcorp.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Emails/NewCaseNotificationToCustomerCaseTeam</template>
    </alerts>
    <alerts>
        <fullName>SF_New_Assignment_Notification</fullName>
        <description>New Assignment Notification - Salesforce Queue</description>
        <protected>false</protected>
        <recipients>
            <recipient>Salesforce</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>customerservice@synergentcorp.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Emails/SUPPORTNewassignmentnotification</template>
    </alerts>
    <alerts>
        <fullName>Submitter_New_assignment_notification</fullName>
        <description>New assignment notification - Submitter</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>customerservice@synergentcorp.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Emails/SUPPORTNewassignmentnotification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign_to_Production_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Ops_Production_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Production Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_Awaiting_ECM_Approval</fullName>
        <field>Status</field>
        <literalValue>Awaiting ECM Approval</literalValue>
        <name>Case Status=Awaiting ECM Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_Initial_Change_Review</fullName>
        <field>Status</field>
        <literalValue>Initial Change Review</literalValue>
        <name>Case Status =Initial Change Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_New</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Case Status = New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_Production_Implementation</fullName>
        <field>Status</field>
        <literalValue>Production Implementation</literalValue>
        <name>Case Status=Production Implementation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetStatusClosed</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>SetStatusClosed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetStatusFinal</fullName>
        <field>Status</field>
        <literalValue>Final Change Review Approval</literalValue>
        <name>SetStatusFinal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetStatusRejected</fullName>
        <field>Status</field>
        <literalValue>Production Change Verification</literalValue>
        <name>SetStatusRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_owner</fullName>
        <field>OwnerId</field>
        <lookupValue>Change_Admin_Approval_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Notify Case Cancelled</fullName>
        <actions>
            <name>Case_Cancelled_Notification_to_Contact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <description>Notification to contact when case cancelled</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Case Closed</fullName>
        <actions>
            <name>Case_Closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Email notification to contact when case closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>When Survey is Attached</fullName>
        <actions>
            <name>Assign_Programmer</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Implement Product</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Survey Attached</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>When Survey is Sent</fullName>
        <actions>
            <name>Attach_Completed_Product_Implementation_Survey</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Implement Product</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Survey Sent</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>When a Product Implementation Case is Approved</fullName>
        <actions>
            <name>Send_Product_Implementation_Survey_to_Credit_Union</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Implement Product</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>SC Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Assign_Programmer</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Assign Programmer</subject>
    </tasks>
    <tasks>
        <fullName>Attach_Completed_Product_Implementation_Survey</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Attach Completed Product Implementation Survey</subject>
    </tasks>
    <tasks>
        <fullName>Send_Product_Implementation_Survey_to_Credit_Union</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Product Implementation Survey to Credit Union</subject>
    </tasks>
</Workflow>
