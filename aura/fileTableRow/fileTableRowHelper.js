({
    bytesToSize: function(bytes) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Byte';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
    },
    
    editRecord : function(component, event, helper) {
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": component.get("v.row.fileId")
        });
        editRecordEvent.fire();
    },
    
    // 060619 - T - 00185 - Method to delete the file.
    deleteSelFile : function(component, event, helper){
        var rowId = component.get("v.row.fileId");
        console.log("rowid18-- : "+rowId);
        var action = component.get("c.deleteFile");
        action.setParams({
            "fileId": rowId
        });    
        
        action.setCallback(this, function(response) {
            var state =  response.getState();
            if(state == "SUCCESS") {
                var resultObj = response.getReturnValue();
                console.log('--resultObj--',resultObj);
                helper.showDelMsgToastfire(resultObj);
                component.set("v.deleteFile",false);
            }
        });
        
        $A.enqueueAction(action);
        //$A.get('e.force:refreshView').fire();
    },
        
    // 070619 - T - 00185 - Method to display success message.
    showDelMsgToastfire : function(msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success',
            message: msg,
            duration:'3000',
            type: 'Success'
        });
        toastEvent.fire();
        },

        /* 130619 - T - 00185 - to get the current url if it is blank that will internal user url.*/
        getCurrentUrl : function(component, event, helper){
            var action = component.get("c.getPathPrefix");
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var storeResponse = response.getReturnValue();
                    console.log('respone---',storeResponse);
                    if($A.util.isEmpty(storeResponse)  ||  storeResponse == null){
                        component.set('v.displayColumn',true);
                    } else {
                         component.set('v.displayColumn',false);
                    }
                }
            });
            $A.enqueueAction(action);

        },
    
    /* 190619 - T - 00185 - to get the owner of the file.*/
    fileReviewForUser : function(cmp, event, helper){
            var fileOwner = cmp.get("v.row.ownerId");
            console.log('--fileOwner--',fileOwner);
                
            var action = cmp.get("c.getfileUse");
        
        action.setParams({
                'fileOwnerId' : fileOwner
                });
        
        action.setCallback(this,(response)=>{
            const state = response.getState();
            console.log(state,response.getReturnValue());
                if(state === 'SUCCESS'){
                	var response = response.getReturnValue();
                	console.log('reponse--',response);
            		cmp.set("v.fileReviewUser",response);
                }
        });
        
        $A.enqueueAction(action);
      },
    
})