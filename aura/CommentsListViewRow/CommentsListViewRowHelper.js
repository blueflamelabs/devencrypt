({

    
     editRecord : function(component, event, helper) {
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": component.get("v.row.CaseCommentId")
        });
        editRecordEvent.fire();
    },
    
    getCaseCommentUser : function(cmp, event, helper){
            var CreateByName = cmp.get("v.row.createByName");
            console.log('--caseCommentOwner--',CreateByName);
                
            var action = cmp.get("c.getUserOfCaseComment");
        
        action.setParams({
                'caseOwnerName' : CreateByName
                });
        
        action.setCallback(this,(response)=>{
            const state = response.getState();
            console.log(state,response.getReturnValue());
                if(state === 'SUCCESS'){
                	var response = response.getReturnValue();
                	console.log('reponse--',response);
            		cmp.set("v.caseCommentUser",response);
                }
        });
        
        $A.enqueueAction(action);
      },
    
    
    
})