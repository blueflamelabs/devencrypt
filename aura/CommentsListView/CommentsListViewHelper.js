({
	doInit : function(component,event) 
    {
         //var caseId = component.get("v.recordId");
		//console.log('entered casecomments getData, caseId - '+ caseId); 
		       
        var action = component.get('c.GetCaseCommentCaseMonitoring');
        action.setParams({
            "caseId": component.get("v.parentId")
        });
        action.setCallback(this, $A.getCallback(function (response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") {
                var commentslist = response.getReturnValue();

                //cmp.set("v.commentsdata", commentslist);
                
                component.set("v.rowsToDisplay",commentslist);
               	var element;
                for (var i = 0; i < commentslist.length; i++) 
                {
                    element = commentslist[i];
                    component.set("v.isCommunityUser", element.IsCommunityUser);
				}                

                

            } else if (state === "ERROR") {
                console.log('state - '+ state);
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
        
	},
    
    SetIsCommunityUser : function(component,event) 
    {
		       
        var action = component.get('c.GetIsCommunityUser');
        //action.setParams({
        //    "caseId": component.get("v.parentId")
        //});
        action.setCallback(this, $A.getCallback(function (response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") 
    		{
                var IsCommunityUser = response.getReturnValue();

                 component.set("v.isCommunityUser", IsCommunityUser);

            } 
 			else if (state === "ERROR") 
			{
                console.log('state - '+ state);
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
        
	},
    setColumnWrapper: function(component,event) {
		 const internalcolumns = [
            //{
            ///    'label' : 'Case Comment Name',
            //    'apiName' : 'Name',
            //    'type':'reference',
			//     'width' : '20%'
            //},
            {
                'label' : 'Review Complete',
                'apiName' : 'CommentReviewComplete',
                'type':'boolean',
                'width' : '10%'
            },
            {
                'label' : 'Public',
                'apiName' : 'PublicFlag',
                'type':'boolean',
                'width' : '10%'
            },

            {
                'label' : 'Owner',
                'apiName' : 'ownerName',
                'type':'reference',
                'width' : '10%'
            },
             {
                'label' : 'Created Date',
                'apiName' : 'CreatedDate',
                'type':'string',
                'width' : '10%'
            },
            {
                'label' : 'Comment Body',
                'apiName' : 'CommentBody',
                'type':'string',
                'width' : '40%'
            },
            
           
           
        ];
 		 const externalcolumns = [
            {
                'label' : 'Owner',
                'apiName' : 'ownerName',
                'type':'reference',
                'width' : '25%'
            },
             {
                'label' : 'Created Date',
                'apiName' : 'CreatedDate',
                'type':'string',
                'width' : '25%'
            },
            {
                'label' : 'Comment Body',
                'apiName' : 'CommentBody',
                'type':'string',
                'width' : '50%'
            },
            
           
           
        ];
        
        var bIsCommunityUser = component.get("v.isCommunityUser");   
        if (bIsCommunityUser == false) 
        {             
        	//component.set("v.columns", externalcolumns);
            component.set("v.columns", internalcolumns);
        }
        else
        {
        	component.set("v.columns", externalcolumns);
        }
        
    },
         sortBy: function(component,event) {
        let sortProperties = component.get("v.sortProperties"),
            records = component.get("v.rowsToDisplay"),
            sortAsc = sortProperties.sortOrder == 'asc' ? true : false,
            field = sortProperties.sortField;
         	
       
        records.sort(function(a,b){
            let avalue = (a[field] != 0 && a[field])  ? a[field] : null;
             let bvalue = (b[field] != 0 && b[field]) ? b[field] : null;
            
            if(avalue == "" || avalue == null) return 1;
            if(bvalue == "" || bvalue == null) return -1;

            if(field == 'OpportunityTotal' || field == 'MinimumDriverCount' || field == 'actualPriceIncrease'){
                avalue = (avalue != 0 && avalue) ? parseInt(avalue) : null; 
                bvalue = (bvalue != 0 && bvalue) ? parseInt(bvalue) : null;
            }
            console.log(avalue,bvalue);
          
                let t1 = avalue == bvalue ,
                    t2 = (avalue != 0 && !avalue && bvalue) || (avalue < bvalue);
                return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
        });
         
        component.set("v.rowsToDisplay", records);
         
    }, 
    
         submitCreateDetails: function(component, event, helper) 
    {
      console.log('submitCreateDetails  ' + component.get("v.parentId")  );
        
		      
      var caseId = component.get("v.parentId")  ;
      var varcommentBody = component.get("v.newCommentBody");
      var varpublished = component.get("v.newIsPublic");
  
        // Prepare the action to create new case comment
        var addCommentAction = component.get('c.addCaseComment');
        addCommentAction.setParams({
            "commentBody": varcommentBody,
            "published": varpublished,
            "caseId": caseId
        });
        
        // Configure the response handler for the action
        addCommentAction.setCallback(this, $A.getCallback(function (response) 
        {
            var state = response.getState();
            if(state === "SUCCESS") 
            {
               //$A.get("e.force:refreshView").fire();
               // 
                 
                 helper.doInit(component);
            }
            else if (state === "ERROR") 
            {
                console.log('Problem saving contact, response state: ' + state);
                
            }
            else 
            {
                console.log('Unknown problem, response state: ' + state);
                
            }
            
        }));
        $A.enqueueAction(addCommentAction);
      	component.set("v.isCreateModalOpen", false);
         
       
   },

})