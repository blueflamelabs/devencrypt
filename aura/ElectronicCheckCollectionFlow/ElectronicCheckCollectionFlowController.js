({
	doInit: function(component, event, helper) 
    {
            
       // Set the validate attribute to a function that includes validation logic
       component.set('v.validate', function() 
       {
           var userInputSubject = component.get('v.objBatch');
           //if(userInputSubject && userInputSubject.Trace_Number__c.length>0) 

           if(userInputSubject) 
           {
               //
               var strTrace_Number__c = userInputSubject.Branch_Location_Text__c;
               if (strTrace_Number__c)
               {
                   if (strTrace_Number__c.length>0)
                   {
                       //return { isValid: true };               
                   }
                   else
                   {
                       return { isValid: false, errorMessage: 'A Branch Location is required.' };
                   }
               }
               else
               {
                      return { isValid: false, errorMessage: 'A Branch Location is required.' };
               }
               //
               var strDraft_Number__c = userInputSubject.Batch_Number__c;
               if (strDraft_Number__c)
               {
                   if (strDraft_Number__c.length>0)
                   {
                       //return { isValid: true };               
                   }
                   else
                   {
                       return { isValid: false, errorMessage: 'A Batch Number is required.' };
                   }
               }
               else
               {
                      return { isValid: false, errorMessage: 'A Batch Number is required.' };
               }
               //
               var strAccount_Number__c = userInputSubject.Reason_For_Batch_Action__c;
               if (strAccount_Number__c)
               {
                   if (strAccount_Number__c.length>0)
                   {
                       //return { isValid: true };               
                   }
                   else
                   {
                       return { isValid: false, errorMessage: 'A Reason For Batch Action is required.' };
                   }
               }
               else
               {
                      return { isValid: false, errorMessage: 'A Reason For Batch Action is required.' };
               }
               //
               var strAmount__c = userInputSubject.Batch_Dollar_Amount__c;
               if (strAmount__c)
               {
                   if (strAmount__c > 0)
                   {
                       //return { isValid: true };               
                   }
                   else
                   {
                       return { isValid: false, errorMessage: 'A Batch Dollar amount is required.' };
                   }
               }
               else
               {
                      return { isValid: false, errorMessage: 'A Batch Dollar amount is required.' };
               }
				// made it here, should be ok
				//return { isValid: true };               

           }
           else 
           {
               // If the component is invalid...
               return { isValid: false, errorMessage: 'A value is required.' };
           }
           ////////////////////////////////////////////////////////////////////////////////////////////////////////////
           var varAction = component.get("v.radioGrpValue");
            
            if ((varAction == 'Item Deletion')|| (varAction == 'Item Correction'))
            {

           		var userInputSubject = component.get('v.objBatchItem');
           		//if(userInputSubject && userInputSubject.Trace_Number__c.length>0) 

           		if(userInputSubject) 
           		{
               //
               var strTrace_Number__c = userInputSubject.Last_Four_of_Item_Account_Number__c;
               if (strTrace_Number__c)
               {
                   if (strTrace_Number__c.length>0)
                   {
                       //return { isValid: true };               
                   }
                   else
                   {
                       return { isValid: false, errorMessage: 'Last four of account number is required.' };
                   }
               }
               else
               {
                      return { isValid: false, errorMessage: 'Last four of account number is required.' };
               }
               //
               var strDraft_Number__c = userInputSubject.Routing_Number__c;
               if (strDraft_Number__c)
               {
                   if (strDraft_Number__c.length>0)
                   {
                       //return { isValid: true };               
                   }
                   else
                   {
                       return { isValid: false, errorMessage: 'A routing number is required.' };
                   }
               }
               else
               {
                      return { isValid: false, errorMessage: 'A routing number is required.' };
               }
               //
               var strAccount_Number__c = userInputSubject.Check_Number__c;
               if (strAccount_Number__c)
               {
                   if (strAccount_Number__c.length>0)
                   {
                       //return { isValid: true };               
                   }
                   else
                   {
                       return { isValid: false, errorMessage: 'A check number is required.' };
                   }
               }
               else
               {
                      return { isValid: false, errorMessage: 'A check number is required.' };
               }
               //
               var strAmount__c = userInputSubject.Amount__c;
               if (strAmount__c)
               {
                   if (strAmount__c > 0)
                   {
                       //return { isValid: true };               
                   }
                   else
                   {
                       return { isValid: false, errorMessage: 'An amount is required.' };
                   }
               }
               else
               {
                      return { isValid: false, errorMessage: 'An amount is required.' };
               }
               var strAmount__c = userInputSubject.Correct_Amount__c;
               if (strAmount__c)
               {
                   if (strAmount__c > 0)
                   {
                       //return { isValid: true };               
                   }
                   else
                   {
                       return { isValid: false, errorMessage: 'A correct amount is required.' };
                   }
               }
               else
               {
                      return { isValid: false, errorMessage: 'A correct amount is required.' };
               }
               var strAmount__c = userInputSubject.New_Batch_Total__c;
               if (strAmount__c)
               {
                   if (strAmount__c > 0)
                   {
                       //return { isValid: true };               
                   }
                   else
                   {
                       return { isValid: false, errorMessage: 'A new batch total is required.' };
                   }
               }
               else
               {
                      return { isValid: false, errorMessage: 'A new batch total is required.' };
               }
				// made it here, should be ok
				//return { isValid: true };               

           }
           		else 
           		{
               		// If the component is invalid...
               		return { isValid: false, errorMessage: 'A value is required.' };
          		 }
            }
				// made it here, should be ok
				return { isValid: true };               
               //return { isValid: false, errorMessage: 'test only made to end.' };

       })

       
       //Date_Item_Cleared__c

 


    },
})