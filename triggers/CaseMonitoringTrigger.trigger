trigger CaseMonitoringTrigger on Case_Monitoring__c (before insert,after insert, after update)
{
    CaseMonitoringHandler obj= new CaseMonitoringHandler(Trigger.new);
	CaseMonitoringHandler objHandler = new CaseMonitoringHandler(Trigger.new, Trigger.oldMap);
    
    Switch on Trigger.operationType
    {

        //when BEFORE_INSERT 
        //{
            //obj.beforeInsert();
        //}
        //when AFTER_INSERT 
       // {
            //System.debug('After insert called');
            //obj.afterInsert();
       // }
        when AFTER_UPDATE 
        {
            System.debug('After update called');
             obj.afterUpdate();
            //Pass list of case comment records and Trigger.OldMap
            //CaseCommentHandler objHandler = new CaseCommentHandler(Trigger.new, Trigger.oldMap);
            //objHandler.afterUpdate();
        }
        
    }

}