trigger trgr_Case_SetOwner on Case (before update) 
{
    for(Case obj : trigger.new)
    {
        if (Trigger.oldmap.get(obj.id).Status != Trigger.newmap.get(obj.id).Status)
        {
          if ((Trigger.oldmap.get(obj.id).Status == 'Final Change Review Approval') &&  (Trigger.newmap.get(obj.id).Status == 'Production Change Verification' ))
          {
            	obj.OwnerId = obj.CreatedById; 
          }
          if ((Trigger.oldmap.get(obj.id).Status == 'Final Change Review Approval') &&  (Trigger.newmap.get(obj.id).Status == 'Closed' ))
          {
            	obj.OwnerId = obj.CreatedById; 
          }

        }
    }

}