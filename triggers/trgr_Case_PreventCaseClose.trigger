trigger trgr_Case_PreventCaseClose on Case (before Update) 
{
 /*
        A Variable of Collection Type: Set
        that will hold the IDs of all the Closed
        Case records in the Current Trigger Context
    */
    Set<Id> setCases = new Set<Id>();

    for(Case cs : trigger.new)
        if(cs.Status == 'Closed')
            setCases.add(cs.Id);


   

    Map<Id, List<Case>> mapCaseToChildCase = new Map<Id, List<Case>>();
 for(
        Case iCase :
        [
            SELECT
                Id, ParentId
            FROM
                Case
            WHERE 
                ParentId IN :setCases AND
                Status != 'Closed'
        ]
    )
    {
        if(!mapCaseToChildCase.containsKey(iCase.ParentId))
        {
            mapCaseToChildCase.put(
                iCase.ParentId,
                new List<Case>
                {
                    iCase
                }
            );
        }
        else
        {
            mapCaseToChildCase.get(
                iCase.ParentId
            ).add(iCase);
        }
    }
   
    
    Map<Id, List<Task>> mapCaseToTasks = new Map<Id, List<Task>>();

    for(
        Task iTask :
        [
            SELECT
                Id, WhatId
            FROM
                Task
            WHERE 
                WhatId IN :setCases AND
                Status != 'Completed'
        ]
    )
    {
        if(!mapCaseToTasks.containsKey(iTask.WhatId))
        {
            mapCaseToTasks.put(
                iTask.WhatId,
                new List<Task>
                {
                    iTask
                }
            );
        }
        else
        {
            mapCaseToTasks.get(
                iTask.WhatId
            ).add(iTask);
        }
    }
    
   Map<Id, List<Change_Management_Phase__c>> mapCaseToPhases = new Map<Id, List<Change_Management_Phase__c>>();

    for(
        Change_Management_Phase__c iPhase :
        [
            SELECT
                Id, Case__r.Id
            FROM
                Change_Management_Phase__c
            WHERE 
                Case__r.Id IN :setCases AND
                Status__c != 'Complete'
        ]
    )
    {
        if(!mapCaseToPhases.containsKey(iPhase.Case__r.Id))
        {
            mapCaseToPhases.put(
                iPhase.Case__r.Id,
                new List<Change_Management_Phase__c>
                {
                    iPhase 
                }
            );
        }
        else
        {
            mapCaseToPhases.get(
                iPhase.Case__r.Id
            ).add(iPhase);
        }
    }
  
    
    
////////////////////////////////////////////////////////////////////////////////////////
        Map<Id, List<CaseHistory>> mapCaseToCaseHistory = new Map<Id, List<CaseHistory>>();

    //for(
    //    CaseHistory iCaseHistory :
   //    [            
   //         SELECT Id, CaseId, Field, NewValue, OldValue FROM CaseHistory WHERE CaseId IN :setCases AND
   //             Field = 'Status'
   //         // error on this crap
   //         //AND NewValue = 'Invoicing In Progress'
   //     ]
   // )
    //{
    //    if(!mapCaseToCaseHistory.containsKey(iCaseHistory.CaseId))
    //    {
    //        if (iCaseHistory.NewValue == 'Invoicing In Progress')
    //        {
    //        mapCaseToCaseHistory.put(
    //            iCaseHistory.CaseId,
    //            new List<CaseHistory>
    //            {
    //                iCaseHistory
    //            }
    //        );
    //        }
    //    }
    //    else
   //     {
   //         if (iCaseHistory.NewValue == 'Invoicing In Progress')
   //         {
   //           mapCaseToCaseHistory.get(iCaseHistory.CaseId).add(iCaseHistory);
   //         }
   //     }
   // }
////////////////////////////////////////////////////////////////////////////////////////
    Id CMRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change Management').getRecordTypeId();
    
    for(Case cs : trigger.new)
    {
        if(cs.Status == 'Cancelled')
        {
            Profile p = [select id,Name from Profile where id=:Userinfo.getProfileid()];
            if (p.Name  != 'System Administrator')
            {            
                if (Userinfo.getUserId() != cs.CreatedById)
                {
                    cs.addError(
                        'You cannot cancel this case. ' + 
                        'Only the user that created the case may cancel.');
                }
            }
        }

        
        if(cs.Status == 'Closed')
        {
            if (cs.RecordTypeId == CMRecordTypeId)
            {
                if (Trigger.oldmap.get(cs.id).Status != 'Final Change Review Approval')
                {
                    cs.addError(
                        'You cannot Close this case. ' + 
                        'This case must go thru Final Change Review Approval.');
                }
            }
            
            if(
                mapCaseToPhases.containsKey(cs.Id) &&
                mapCaseToPhases.get(
                    cs.Id
                ).size() > 0
            )
            {
                cs.addError(
                    'You cannot Close this case. ' + 
                    'There are Open Phases under this Case.'
                    
                );
            }

            if(
                mapCaseToTasks.containsKey(cs.Id) &&
                mapCaseToTasks.get(
                    cs.Id
                ).size() > 0
            )
            {
                cs.addError(
                    'You cannot Close this case. ' + 
                    'There are Open Tasks under this Case.'
                    
                );
            }
            if(
                mapCaseToChildCase.containsKey(cs.Id) &&
                mapCaseToChildCase.get(
                    cs.Id
                ).size() > 0
            )
            {
                cs.addError(
                    'You cannot Close this case. ' + 
                    'There are Open Child Cases under this Case.'  
                );
            }
            //if(
            //    mapCaseToCaseHistory.containsKey(cs.Id) &&
            //    mapCaseToCaseHistory.get(
            //        cs.Id
            //    ).size() == 0
            //)
           // {
                // this may not apply to all case record types, hold for now
                //cs.addError(
                //    'You cannot Close this case. ' + 
                //    'This Case has not been sent to billing based on Invoicing In Progress status has not been selected before a close.'
                    
                //);
            //}


        }
    }
}