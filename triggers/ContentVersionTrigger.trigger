trigger ContentVersionTrigger on ContentVersion (before insert,after insert,after update) {
    
    // 070619 - T-00209 - Added debugs
    //System.debug('=== Trigger called == ');
    ContentVersionHandler contentVersionHand = new ContentVersionHandler(Trigger.new);
    // 070619 - T-00209 - Added debugs
    //System.debug('=== Trigger called == ');
    
    Switch on Trigger.operationType{

        when BEFORE_INSERT {
            //System.debug('Trigger.New======='+Trigger.New);
            contentVersionHand.beforeInsert();
        }
        when AFTER_INSERT {
            contentVersionHand.afterInsert();
        }
        when AFTER_UPDATE {
            //System.debug('Trigger.New in after update======='+Trigger.New);
            //System.debug('Trigger.OldMap in after update======='+Trigger.OldMap);
            ContentVersionHandler objContentVersionHandler = new ContentVersionHandler(Trigger.new, Trigger.oldMap);
            objContentVersionHandler.afterUpdate();
        }
        
    }
    
}